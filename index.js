const express = require('express');
const app = express();

const dialogflow = require('@google-cloud/dialogflow');
const projectId = 'agent-vedant-kgj9';
const intentClient = new dialogflow.IntentsClient();

app.use('/', async (req, res) => {

  res.setHeader('Access-Control-Allow-Origin', '*');

  try {

    const projectAgentPath = intentClient.projectAgentPath(projectId);
    const request = {
      parent: projectAgentPath
    };

    const [response] = await intentClient.listIntents(request);
    res.status(200).send(response);

  } catch (err) {
    console.log(err);
    res.status(500).end();
  }
})

app.listen(process.env.PORT || 5000, () => {
  console.log('Server has been started');
})