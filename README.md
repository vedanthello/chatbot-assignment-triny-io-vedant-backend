# About this repo:
- This repo attempts to provide a solution to the assignment, given by triny.io 
- Uses Node.js client library for Dialogflow
- Fetches list of intents from an agent and, sends the list as a response

# How to use this repo:
1. Change line 5 in file ./index.js, by putting your own project Id
2. Download the service account key, in JSON format, from Google Cloud platform
3. Save the JSON key file in this repo, and name the file as google-credentials.json
4. Export the JSON file as an environmnetal variable. If you are using Git Bash, this can be done by running the command: 
export GOOGLE_APPLICATION_CREDENTIALS=./google-credentials.json
5. Finally, npm start